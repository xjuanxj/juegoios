//
//  ViewController.swift
//  JuegoIOS
//
//  Created by Juan Sebastian Benavides Cardenas on 13/11/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblAleatorio: UILabel!
    @IBOutlet weak var lblPuntaje: UILabel!
    @IBOutlet weak var lblRonda: UILabel!
    @IBOutlet weak var sliderJuego: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func botonJugar(_ sender: Any) {
    }
    
    @IBAction func botonReinicio(_ sender: Any) {
    }
    
    @IBAction func botonInfo(_ sender: Any) {
    }
    
}

